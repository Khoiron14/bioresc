<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bioresc</title>

    <link rel="icon" href="{{ asset('images/component/logo1.png') }}">

    @include('partials.style')
</head>

<body>
    @include('components.header')

    <div class="row min-h-fullscreen center-vh p-20 m-0">
        <div class="col-12">
            <div class="card card-shadowed px-50 py-30 w-400px mx-auto" style="max-width: 100%">
                <h5>Buat Toko Anda!</h5>
                <br>

                <form class="form-type-material" method="POST" action="{{ route('store.store') }}">
                    @csrf

                    <div class="form-group">
                        <label>Nama Toko</label>
                        <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}"
                            type="text" required>

                        @if ($errors->has('name'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('name') }}</strong>
                        </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Kota</label>
                        <input class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city') }}"
                            type="text" required>

                        @if ($errors->has('city'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('city') }}</strong>
                        </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label>Alamat</label>
                        <input class="form-control{{ $errors->has('location') ? ' is-invalid' : '' }}" name="location"
                            type="text" required>

                        @if ($errors->has('location'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('location') }}</strong>
                        </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label>De]skripsi</label>
                        <textarea name="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" required></textarea>

                        @if ($errors->has('description'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('description') }}</strong>
                        </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <button class="btn btn-bold btn-block btn-primary" type="submit">Buat Toko</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @include('partials.script')
</body>

</html>