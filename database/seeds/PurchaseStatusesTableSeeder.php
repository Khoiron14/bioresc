<?php

use Illuminate\Database\Seeder;
use App\Models\Purchases\PurchaseStatus;

class PurchaseStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PurchaseStatus::create(['name' => 'confirmed']);
        PurchaseStatus::create(['name' => 'pending']);
        PurchaseStatus::create(['name' => 'cancelled']);
    }
}
