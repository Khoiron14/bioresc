<?php

use Illuminate\Database\Seeder;
use App\Models\Users\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!File::exists('public/images/user')) {
            File::makeDirectory('public/images/user');
        }

        $user = factory(User::class)->create([
            'email' => 'admin@example.com',
        ])
        ->assignRole('admin');

       $user = factory(User::class)->create([
            'email' => 'seller@example.com',
        ])
        ->assignRole('user', 'seller');

        $user = factory(User::class)->create([
            'email' => 'user@example.com',
        ])
        ->assignRole('user');
    }
}
