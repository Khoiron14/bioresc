<?php

namespace App\Http\Controllers\Users;

use App\Http\Requests\UserAddressRequest as Request;
use App\Http\Controllers\Controller;
use App\Models\Users\UserAddress as Address;

class AddressController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store(Request $request)
    {
        if ($request->has('is_default') && auth()->user()->addresses->count() != 0) {
            $address = auth()->user()->addresses()->whereIs_default(1)->first();
            $address->update(['is_default' => 0]);
        }

        auth()->user()->addresses()->create($request->all());

        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update(Request $request, Address $address)
    {
        if (auth()->user()->addresses->count() > 1) {
            if ($request->has('is_default') && !$address->is_default) {
                auth()->user()->addresses()->whereIs_default(1)->first()->update(['is_default' => 0]);
                $address->update($request->all());
            } else if (!$request->has('is_default') && $address->is_default) {
                auth()->user()->addresses()->whereIs_default(0)->first()->update(['is_default' => 1]);
                $address->update(array_merge($request->all(), ['is_default' => 0]));
            }
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy(Address $address)
    {
        $address->delete();

        if ($address->is_default && auth()->user()->addresses->count() != 0) {
            $address = auth()->user()->addresses()->whereIs_default(0)->first();
            $address->update(['is_default' => 1]);
        }

        return redirect()->back();
    }
}
