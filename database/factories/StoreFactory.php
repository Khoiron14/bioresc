<?php

use Faker\Generator as Faker;
use App\Models\Store;

$factory->define(Store::class, function (Faker $faker) {
    $name = $faker->company;

    return [
        'name' => $name,
        'domain' => str_slug($name),
        'location' => $faker->streetAddress,
        'city' => $faker->city,
        'description' => $faker->text(100)
    ];
});
