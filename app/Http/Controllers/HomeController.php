<?php

namespace App\Http\Controllers;

use App\Models\Items\Item;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     */
    public function index()
    {
        $items = Item::latest()->paginate(12);

        return view('home', compact('items'));
    }

}
