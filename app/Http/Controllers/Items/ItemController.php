<?php

namespace App\Http\Controllers\Items;

use Storage;
use App\Http\Controllers\Controller;

use App\Models\Store;
use App\Models\Items\Item;
use App\Models\Items\ItemCategory as Category;
use App\Models\Purchases\PurchaseType as Type;

class ItemController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     */
    public function store(Store $store)
    {
        $item = auth()->user()->store->items()->create([
            'name' => request()->name,
            'price' => request()->price,
            'description' => request()->description,
            'stock_status' => request()->stock_status,
            'slug' => str_slug(request()->name),
            'unit_type' => request()->unit_type
        ]);

        $item->images()->createMany(collect(request()->file('images'))->map(function($image) {
             return ['path' => basename($image->store('images/item'))];
        })->toArray());

        $item->categories()->attach(request()->item_categories);

        $item->purchaseTypes()->attach(request()->purchase_types);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     */
    public function show(Store $store, Item $item)
    {
        $purchaseTypes = Type::all();
        $itemCategories = Category::all();

        return view('item.show', compact('item', 'purchaseTypes', 'itemCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update(Store $store, Item $item)
    {
        $item->update([
            'name' => request()->name,
            'price' => request()->price,
            'description' => request()->description,
            'stock_status' => request()->stock_status,
            'slug' => str_slug(request()->name),
            'unit_type' => request()->unit_type
        ]);

        if (request()->file('images') != null) {
            Storage::delete(collect($item->images()->get())->map(function($image) use ($item) {
                return $item::IMAGE_PATH . $image->path;
            }));

            $item->images()->delete();

            $item->images()->createMany(collect(request()->file('images'))->map(function($image) {
                return ['path' => basename($image->store('images/item'))];
           })->toArray());
        }

        $item->categories()->sync(request()->item_categories);

        $item->purchaseTypes()->sync(request()->purchase_types);

        return redirect()->route('item.show', [$item->store, $item]);
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy(Store $store, Item $item)
    {
        $item->delete();

        return redirect()->route('home');
    }
}
