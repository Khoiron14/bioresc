<?php

namespace App\Models\Items;

use Illuminate\Database\Eloquent\Model;

use App\Models\Items\Item;

class ItemCategory extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function items()
    {
        return $this->belongsToMany(Item::class, 'category_item', 'category_id', 'item_id');
    }
}
