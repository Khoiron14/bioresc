<?php

use Faker\Generator as Faker;
use App\Models\Items\Item;
use App\Models\Items\ItemCategory as Category;
use App\Models\Purchases\PurchaseType;
use App\Models\Image;

$factory->define(Item::class, function (Faker $faker) {
    $name = $faker->text(10);

    return [
        'name' => $name,
        'price' => $faker->randomNumber(5),
        'description' => $faker->text(200),
        'stock_status' => $faker->boolean(50),
        'slug' => str_slug($name),
        'unit_type' => $faker->sentence(1)
    ];
});

$factory->afterCreating(Item::class, function($item, $faker) {
    $categories = Category::inRandomOrder()->take(2)->get();
    $purchaseTypes = PurchaseType::inRandomOrder()->take(2)->get();
    $images = factory(Image::class, 3)->states('item')->make();

    $item->categories()->attach($categories);
    $item->purchaseTypes()->attach($purchaseTypes);
    $item->images()->saveMany($images);
});
