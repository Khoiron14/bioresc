<?php

namespace App\Models\Users;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

use App\Models\Store;
use App\Models\Users\UserAddress as Address;
use App\Models\Users\UserCart as Cart;
use App\Models\Users\UserMetaData as MetaData;
use App\Models\Users\UserReview as Review;
use App\Models\Items\Item;
use App\Models\Purchases\Purchase;
use App\Models\Image;
use phpDocumentor\Reflection\Types\Boolean;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use HasRoles;

    const IMAGE_PATH = '/images/user/';

    protected $guard_name = 'web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getImage() : string
    {
        $image = $this->image()->first();

        if ($image) {
            return self::IMAGE_PATH . $image->path;
        }

        return str_limit($this->username, 1, '');
    }

    public function isOwner($id) : bool
    {
        if (auth()->user()->id == $id) {
            return true;
        }

        return false;
    }

    public function store()
    {
        return $this->hasOne(Store::class);
    }

    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function carts()
    {
        return $this->hasMany(Cart::class);
    }

    public function metaData()
    {
        return $this->hasOne(MetaData::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function wishlist()
    {
        return $this->belongsToMany(Item::class, 'wishlist');
    }

    public function purchases()
    {
        return $this->hasMany(Purchase::class);
    }

    public function image()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
}
