<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bioresc</title>

    <link rel="icon" href="{{ asset('images/component/logo1.png') }}">

    @include('partials.style')
</head>

<body>
    @include('components.header')

    <div class="row min-h-fullscreen center-vh p-20 m-0">
        <div class="col-12">
            <div class="card card-shadowed px-50 py-30 w-400px mx-auto" style="max-width: 100%">
                <h5>Reset Password</h5>
                <br>

                <form class="form-type-material" method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <div class="form-group">
                        <label>Email Address</label>
                        <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"
                            type="email" required>

                        @if ($errors->has('email'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <button class="btn btn-bold btn-block btn-primary" type="submit">Send Password Reset Link</button>
                    </div>

                    <div class="form-group">
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>

    @include('partials.script')
</body>

</html>
