<?php

namespace App\Models\Purchases;

use Illuminate\Database\Eloquent\Model;

use App\Models\Purchases\Purchase;

class PurchaseStatus extends Model
{
    const CONFIRM = 1;
    const PENDING = 2;
    const CANCEL = 3;

    public $timestamps = false;
    protected $guarded = [];

    public function purchases()
    {
        return $this->hasMany(Purchase::class);
    }
}
