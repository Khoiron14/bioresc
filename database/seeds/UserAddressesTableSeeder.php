<?php

use Illuminate\Database\Seeder;
use App\Models\Users\User;
use App\Models\Users\UserAddress as Address;

class UserAddressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::role('user')->get();

        foreach ($users as $user) {
            factory(Address::class)->create([
                'user_id' => $user->id,
                'is_default' => true
            ]);
        }
    }
}
