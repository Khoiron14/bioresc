<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bioresc</title>

    <link rel="icon" href="{{ asset('images/component/logo1.png') }}">

    @include('partials.style')
</head>

<body data-provide="pace">
    @include('components.header')

    <div class="p-100">
        <div class="row">
            <div class="col-md-4">
                <div class="card w-350px bl-3 border-primary rounded">
                    <div class="card-body p-0">
                        <div class="carousel slide cursor-pointer" id="carousel-3" data-provide="photoswipe" data-ride="carousel">
                            <div class="carousel-inner">
                                @foreach ($item->images()->get() as $key => $image)
                                <div class="carousel-item {{ ($key == 0) ? 'active' : '' }}">
                                    <img src="{{ asset($item::IMAGE_PATH . $image->path) }}" alt="{{ $item->name }}"
                                        style="object-fit:cover">
                                </div>
                                @endforeach
                            </div>

                            <a class="carousel-control-prev" href="#carousel-3" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel-3" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <h1>{{ $item->name }}</h1>

                {{-- category --}}
                <div class="">
                    @foreach ($item->categories()->get() as $category)
                    <span class="badge badge-pill badge-warning">{{ $category->name }}</span>
                    @endforeach
                </div>

                <h3 class="mt-4"><b class="text-success">Rp. {{ number_format($item->price) }}</b> /{{ $item->unit_type
                    }}</h3>

                {{-- stock status --}}
                <p class="fs-18 mt-4"><i class="ti-package"></i> Stock : <b>{{ $item->isAvailable() }}</b></p>

                <form class="d-inline" action="{{ route('wishlist.update', $item) }}" method="POST">
                    @csrf
                    @if (auth()->user())
                    <button name="wishlist_action" value="{{ auth()->user()->wishlist->contains($item) ? 'remove' : 'add' }}"
                        type="submit" class="btn btn-label btn-info" data-toggle="tooltip" data-placement="bottom"
                        title="{{ auth()->user()->wishlist->contains($item) ? 'Remove' : 'Add' }} to wishlist">
                        <label>
                            <i class="ti-{{ auth()->user()->wishlist->contains($item) ? 'close' : 'plus' }} p-12"></i>
                        </label>
                        Wishlist
                    </button>
                    @else
                    <button name="wishlist_action" value="add" type="submit" class="btn btn-label btn-info" data-toggle="tooltip"
                        data-placement="bottom" title="Add to wishlist">
                        <label>
                            <i class="ti-plus p-12"></i>
                        </label>
                        Wishlist
                    </button>
                    @endif
                </form>

                <button class="btn btn-label btn-primary"><label><i class="ti-shopping-cart"></i></label> Beli Sekarang</button>

                <br>

                @if (auth()->user()->isOwner($item->store->user->id))
                <button data-target="#modal-update-item" data-toggle="modal" data class="btn btn-label btn-secandary mt-2"><label><i class="ti-pencil-alt"></i></label> Edit Barang</button>
                <button data-target="#modal-delete-item" data-toggle="modal" data class="btn btn-label btn-danger mt-2"><label><i class="ti-trash"></i></label> Hapus Barang</button>

                <div class="modal fade" id="modal-update-item" tabindex="-1" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title"><i class="ti-pencil-alt"></i> Edit Barang</h5>
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <form action="{{ route('item.update', [$item->store, $item]) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PATCH')
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>Nama Barang</label>
                                        <input name="name" class="form-control" type="text" value="{{ $item->name }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Harga</label>
                                        <input name="price" class="form-control" type="number" min="0" value="{{ $item->price }}"
                                            required>
                                    </div>
                                    <div class="form-group">
                                        <label>Tipe Satuan</label>
                                        <input name="unit_type" class="form-control" type="text" value="{{ $item->price }}" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Status Stok</label>
                                        <select name="stock_status" class="form-control">
                                            <option value="0" {{ $item->stock_status == 0 ? 'selected' : '' }}>Habis</option>
                                            <option value="1" {{ $item->stock_status == 1 ? 'selected' : '' }}>Tersedia</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Deskripsi</label>
                                        <textarea name="description" class="form-control" required>{{ $item->description }}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Foto</label>
                                        <input name="images[]" class="form-control-file" type="file" value="" multiple>
                                    </div>
                                    <div class="form-group">
                                        <label>Kategori</label>
                                        @foreach ($itemCategories as $category)
                                        <div class="checkbox">
                                            <input type="checkbox" name="item_categories[]" value="{{ $category->id }}" {{ $item->categories->contains($category) ? 'checked' : '' }}>
                                            {{ $category->name }}
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="form-group">
                                        <label>Tipe Pembayaran</label>
                                        @foreach ($purchaseTypes as $type)
                                        <div class="checkbox">
                                            <input type="checkbox" name="purchase_types[]" value="{{ $type->id }}" {{ $item->purchaseTypes->contains($type) ? 'checked' : '' }}>
                                            {{ $type->name }}
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-bold btn-pure btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-bold btn-pure btn-primary">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="modal modal-center fade" id="modal-delete-item" tabindex="-1" role="dialog"
                    aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form action="{{ route('item.destroy', [$item->store, $item]) }}" method="post" class="d-inline">
                                <div class="modal-header">
                                    <h5 class="modal-title">Konfirmasi</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @csrf
                                    @method('DELETE')
                                    Anda yakin ingin menghapus '{{ $item->name }}'?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-danger">Hapus</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @endif

                <p class="fs-18 mt-4"><i class="ti-wallet"></i> Tipe Pembayaran :</p>
                <ul>
                    @foreach ($item->purchaseTypes()->get() as $type)
                    <li class="fs-16">{{ $type->name }}</li>
                    @endforeach
                </ul>
            </div>

            <div class="col-md-4">
                <div class="card w-250px pull-right">
                    <div class="card-body py-50 text-center">
                        @if (strlen($item->store->getImage()) > 1)
                        <img class="avatar avatar-xxl" src="{{ $item->store->getImage() }}">
                        @else
                        <span class="avatar avatar-xxl bg-dark fs-25">{{ $item->store->getImage() }}</span>
                        @endif
                        <h5 class="mt-3 mb-1"><a class="hover-primary" href="{{ route('store.index', $item->store) }}">{{
                                $item->store->name }}</a></h5>
                        <p class="text-fade"><i class="ti-location-pin"></i> {{ $item->store->city }}</p>
                    </div>

                    <div class="flexbox flex-justified bt-1 border-light py-12 bg-lightest text-center">
                        <a class="text-muted" href="#">
                            <i class="ti-archive fs-35"></i><br>
                            {{ $item->store->items()->count() }} Barang
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="card mt-4">
            <div class="card-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#deskripsi">Deskripsi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#ulasan">Ulasan</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content fs-16">
                    <div class="tab-pane fade active show" id="deskripsi">
                        {{ $item->description }}
                    </div>
                    <div class="tab-pane fade" id="ulasan">
                        Professionally embrace proactive value whereas customized solutions. Monotonectally formulate
                        high standards in e-business with cost effective ideas. Objectively cultivate maintainable.
                    </div>
                </div>

            </div>
        </div>
    </div>

    @include('partials.script')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
</body>

</html>