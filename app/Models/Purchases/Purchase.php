<?php

namespace App\Models\Purchases;

use Illuminate\Database\Eloquent\Model;

use App\Models\Users\User;
use App\Models\Users\UserAddress as Address;
use App\Models\Items\Item;
use App\Models\Purchases\PurchaseType as Type;
use App\Models\Purchases\PurchaseStatus as Status;

class Purchase extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }
}
