<?php

use Faker\Generator as Faker;
use App\Models\Image;

$factory->define(Image::class, function (Faker $faker) {
    return [];
});

$factory->state(Image::class, 'user', function (Faker $faker) {
    return [
        'path' => $faker->image('public/images/user', 500, 500, 'people', false)
    ];
});

$factory->state(Image::class, 'item', function (Faker $faker) {
    return [
        'path' => $faker->image('public/images/item', 500, 500, 'nature', false)
    ];
});