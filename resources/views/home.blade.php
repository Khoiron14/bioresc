<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bioresc</title>

    <link rel="icon" href="{{ asset('images/component/logo1.png') }}">

    @include('partials.style')
</head>

<body>
    <!-- Preloader -->
    <div class="preloader">
        <div class="spinner-dots">
            <span class="dot1"></span>
            <span class="dot2"></span>
            <span class="dot3"></span>
        </div>
    </div>

    @include('components.header')

    <div class="p-100">
        <div class="card-deck row">
            @foreach ($items as $item)
            <div class="col-md-3">
                <div class="card card-shadowed mx-auto rounded">
                    <a href="{{ route('item.show', [$item->store, $item]) }}">
                        <img class="card-img-top h-200px" style="object-fit:cover" src="{{ asset($item::IMAGE_PATH . $item->images()->first()->path) }}" alt="Item image">
                    </a>
                    <div class="card-body">
                        <a href="{{ route('item.show', [$item->store, $item]) }}">
                            <h3 class="card-title b-0 px-0">{{ $item->name }}</h3>
                        </a>
                        <p><b class="text-success">Rp. {{ number_format($item->price) }}</b>  /{{ $item->unit_type }}</p>
                        <small><a class="no-border" href="{{ route('store.index', $item->store) }}">{{ $item->store->name }}</a></small>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    @include('partials.script')
</body>

</html>
