<?php

use Faker\Generator as Faker;
use App\Models\Users\UserMetaData as MetaData;

$factory->define(MetaData::class, function (Faker $faker) {
    return [
        'fullname' => $faker->name,
        'phone' => $faker->phoneNumber,
        'gender' => array_random(['Laki - laki', 'Perempuan']),
        'birth_date' => $faker->date()
    ];
});
