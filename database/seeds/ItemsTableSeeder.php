<?php

use Illuminate\Database\Seeder;
use App\Models\Items\Item;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!File::exists('public/images/item')) {
            File::makeDirectory('public/images/item');
        }

        factory(Item::class, 10)->create([
            'store_id' => 1
        ]);
    }
}
