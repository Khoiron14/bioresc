<?php

namespace App\Http\Middleware;

use Closure;

class IsSeller
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->hasRole('seller')) {
            return redirect()->route('store.index', auth()->user()->store);
        }

        return $next($request);
    }
}
