<?php

use Illuminate\Database\Seeder;
use App\Models\Purchases\PurchaseType;

class PurchaseTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PurchaseType::create(['name' => 'Langsung']);
        PurchaseType::create(['name' => 'Transfer']);
        PurchaseType::create(['name' => 'COD']);
    }
}
