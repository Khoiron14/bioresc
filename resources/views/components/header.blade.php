<header class="topbar">
    <div class="topbar-left">
        <div class="topbar-brand">
            <a href="/" class="logo">
                <img src="{{ asset('images/component/logo2.png') }}" height="42px" alt="logo">
            </a>
        </div>
    </div>

    <div class="topbar-right">
        <div>
            @guest
            <a class="btn btn-sm btn-primary" href="{{ route('login') }}">Login</a>
            <a class="btn btn-sm btn-outline btn-primary" href="{{ route('register') }}">Register</a>
            @else
            <ul class="topbar-btns">
                <li class="dropdown">
                    <span data-toggle="dropdown" aria-expanded="true">
                        <a class="avatar avatar-pill center-vh" href="#">
                            @if (strlen(auth()->user()->getImage()) > 1)
                            <img src="{{ asset(auth()->user()->getImage()) }}">
                            @else
                            <div class="avatar bg-dark">{{ auth()->user()->getImage() }}</div>
                            @endif
                            <span>{{ auth()->user()->username }}</span>
                        </a>
                    </span>

                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: top, left; top: 65px; left: -120px;">
                        <a class="dropdown-item" href="{{ route('user.profile') }}"><i class="ti-user"></i> Profile</a>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                            <i class="ti-power-off"></i>
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
                <div class="topbar-divider"></div>
                <li class="fs-25">
                    @if (auth()->user()->wishlist->count() > 0)
                    <small class="badge badge-pill badge-primary badge-sm fs-10 mt-1" style="position: absolute; right:165px">{{
                        auth()->user()->wishlist->count() }}</small>
                    @endif
                    <span class="center-vh">
                        <a href="{{ route('wishlist.index') }}" class="cursor-pointer hover-primary">
                            <i class="ti-shopping-cart"></i>
                        </a>
                    </span>
                </li>
            </ul>
            @endguest
        </div>
    </div>
</header>