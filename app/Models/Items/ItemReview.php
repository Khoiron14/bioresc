<?php

namespace App\Models\Items;

use Illuminate\Database\Eloquent\Model;

use App\Models\Users\User;
use App\Models\Items\Item;

class ItemReview extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function item()
    {
        return $this->belongsTo(Item::class);
    }
}
