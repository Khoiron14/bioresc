<?php

namespace App\Models\Items;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Users\User;
use App\Models\Users\UserCart as Cart;
use App\Models\Items\Item;
use App\Models\Items\ItemCategory as Category;
use App\Models\Purchases\Purchase;
use App\Models\Purchases\PurchaseType as Type;
use App\Models\Store;
use App\Models\Image;

class Item extends Model
{
    use SoftDeletes;

    const IMAGE_PATH = '/images/item/';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function isAvailable() : string
    {
        return $this->stock_status ? 'Tersedia' : 'Habis';
    }

    public function carts()
    {
        return $this->hasMany(Cart::class);
    }

    public function userWishlist()
    {
        return $this->belongsToMany(User::class, 'wishlist');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_item', 'item_id', 'category_id');
    }

    public function reviews()
    {
        return $this->hasMany(Item::class);
    }

    public function purchases()
    {
        return $this->hasMany(Purchase::class);
    }

    public function purchaseTypes()
    {
        return $this->belongsToMany(Type::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
}
