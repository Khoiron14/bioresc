<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Users\User;
use App\Models\Items\Item;

class Store extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'domain';
    }

    public function getImage() : string
    {
        $image = $this->image()->first();

        if ($image) {
            return '/images/store/' . $image->path;
        }

        return str_limit($this->name, 1, '');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function image()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
}
