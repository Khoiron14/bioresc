<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Items\Item;

class WishlistController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:user']);
    }

    public function index()
    {
        $items = auth()->user()->wishlist;

        return view('user.wishlist', compact('items'));
    }

    public function update(Item $item)
    {
        switch (request()->wishlist_action) {
            case 'add':
                auth()->user()->wishlist()->attach($item);
                break;

            case 'remove':
                auth()->user()->wishlist()->detach($item);
                break;
        }

        return redirect()->back();
    }
}
