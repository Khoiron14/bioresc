<?php

namespace App\Http\Controllers\Stores;

use App\Http\Controllers\Controller;

use App\Models\Store;
use App\Models\Purchases\PurchaseType;
use App\Models\Items\ItemCategory;

class StoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'index']);
        $this->middleware('seller', ['only' => 'create']);
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index(Store $store)
    {
        $items = $store->items()->latest()->get();
        $purchaseTypes = PurchaseType::all();
        $itemCategories = ItemCategory::all();

        return view('store.index', compact('store', 'items', 'purchaseTypes', 'itemCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        return view('store.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store()
    {
        $store = auth()->user()->store()->create([
            'name' => request()->name,
            'domain' => str_slug(request()->name),
            'location' => request()->location,
            'city' => request()->city,
            'description' => request()->description
        ]);

        auth()->user()->assignRole('seller');

        return redirect()->route('store.index', $store);
    }

    /**
     * Display the specified resource.
     *
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update()
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy()
    {
        //
    }
}
