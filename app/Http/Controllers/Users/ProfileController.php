<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Storage;

use App\Models\Users\User;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        return view('user.profile');
    }

    public function update()
    {
        auth()->user()->metaData()->update([
            'fullname' => request()->fullname,
            'phone' => request()->phone,
            'gender' => request()->gender,
            'birth_date' => Carbon::parse(request()->birth_date)
        ]);

        return redirect()->back();
    }

    public function avatar()
    {
        if (request()->image) {
            if (auth()->user()->image) {
                Storage::delete(User::IMAGE_PATH . auth()->user()->image()->first()->path);
                auth()->user()->image()->delete();
            }

            auth()->user()->image()->create([
                'path' => basename(request()->file('image')->store('images/user'))
            ]);
        }

        return redirect()->back();
    }
}
