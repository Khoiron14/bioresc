<?php

use Faker\Generator as Faker;
use App\Models\Users\UserAddress as Address;

$factory->define(Address::class, function (Faker $faker) {
    return [
        'location' => $faker->address,
        'receiver' => $faker->name,
        'phone' => $faker->phoneNumber
    ];
});
