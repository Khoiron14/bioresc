<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bioresc</title>

    <link rel="icon" href="{{ asset('images/component/logo1.png') }}">

    @include('partials.style')
</head>

<body data-provide="pace">
    @include('components.header')

    <div class="p-100">
        <div class="row">
            <div class="col-md-4">
                <div class="card card-body text-center py-70 w-300px">
                    <a>
                        @if (strlen(auth()->user()->getImage()) > 1)
                        <img class="avatar avatar-xxl " src="{{ asset(auth()->user()->getImage()) }}">
                        @else
                        <span class="avatar avatar-xxl bg-dark fs-25">{{ auth()->user()->getImage() }}</span>
                        @endif
                    </a>
                    <a href="" class="badge badge-pill badge-secondary text-dark ml-150 mt-70" data-target="#modal-edit-avatar"
                        data-toggle="modal" style="position: absolute;">
                        <i class="ti-pencil-alt"></i>
                    </a>
                    <h5 class="mt-3 mb-1">{{ auth()->user()->username }}</h5>
                    <p class="text-fade">
                        {{ auth()->user()->email }}
                        <br>
                        <small class="badge badge-pill badge-secondary">
                            {{ auth()->user()->email_verified_at ? 'verified' : 'not verified' }}
                        </small>
                    </p>

                    @if (auth()->user()->hasRole('seller'))
                    <a href="{{ route('store.index', auth()->user()->store) }}" class="btn btn-primary">{{ auth()->user()->store->name }}</a>
                    @else
                    <a href="{{ route('store.create') }}" class="btn btn-primary">Buka Toko</a>
                    @endif

                    <div class="modal modal-center fade" id="modal-edit-avatar" tabindex="-1" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title"><i class="ti-pencil-alt"></i> Ubah Avatar</h5>
                                    <button type="button" class="close" data-dismiss="modal">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <form action="{{ route('user.profile.avatar') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="modal-body center-vh">
                                        @if (strlen(auth()->user()->getImage()) > 1)
                                        <img class="avatar avatar-xxl " src="{{ asset(auth()->user()->getImage()) }}">
                                        @else
                                        <span class="avatar avatar-xxl bg-dark fs-25">{{ auth()->user()->getImage() }}</span>
                                        @endif
                                        <div class="form-group mt-3 ml-3">
                                            <input name="image" class="form-control-file" type="file">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-bold btn-pure btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-bold btn-pure btn-primary">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#detail">Detail</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#alamat">Daftar Alamat</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content fs-16">

                    <div class="tab-pane fade active show" id="detail">
                        <div class="card">
                            <div class="text-muted fs-14 m-3">
                                <a href="" data-target="#modal-edit-profile" data-toggle="modal"><i class="ti-settings"></i>
                                    Edit Profile</a>
                            </div>
                            <div class="media-list media-list-hover media-list-divided">
                                <a class="media media-single">
                                    <span class="title"><strong>Nama Lengkap</strong></span>
                                    <span>{{ auth()->user()->metaData->fullname }}</span>
                                </a>
                                <a class="media media-single">
                                    <span class="title"><strong>No. Handphone</strong></span>
                                    <span>{{ auth()->user()->metaData->phone }}</span>
                                </a>
                                <a class="media media-single">
                                    <span class="title"><strong>Jenis Kelamin</strong></span>
                                    <span class="">{{ auth()->user()->metaData->gender }}</span>
                                </a>
                                <a class="media media-single">
                                    <span class="title"><strong>Tanggal Lahir</strong></span>
                                    <span>{{ \Carbon\Carbon::parse(auth()->user()->metaData->birth_date)->format('d F
                                        Y') }}</span>
                                </a>
                                <a class="media media-single">
                                    <span class="title"><strong>Bergabung</strong></span>
                                    <span>{{ \Carbon\Carbon::parse(auth()->user()->metaData->created_at)->format('d F
                                        Y') }}</span>
                                </a>
                            </div>
                        </div>

                        <div class="modal modal-center fade" id="modal-edit-profile" tabindex="-1" aria-hidden="true"
                            style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title"><i class="ti-map-alt"></i> Ubah Data Diri</h5>
                                        <button type="button" class="close" data-dismiss="modal">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <form action="{{ route('user.profile.update') }}" method="POST">
                                        @csrf
                                        @method('PATCH')
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Nama Lengkap</label>
                                                <input name="fullname" class="form-control" type="text" value="{{ auth()->user()->metaData->fullname }}">
                                            </div>
                                            <div class="form-group">
                                                <label>No. Handphone</label>
                                                <input name="phone" class="form-control" type="text" value="{{ auth()->user()->metaData->phone }}">
                                            </div>
                                            <div class="form-group">
                                                <label>Jenis Kelamin</label>
                                                <div class="dropdown bootstrap-select form-control">
                                                    <select name="gender" class="form-control" data-provide="selectpicker"
                                                        tabindex="-98">
                                                        <option
                                                            {{ (auth()->user()->metaData->gender == 'Laki - laki') ? 'selected' : '' }}>Laki
                                                            - laki</option>
                                                        <option
                                                            {{ (auth()->user()->metaData->gender == 'Perempuan') ? 'selected' : '' }}>Perempuan</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Tanggal Lahir</label>
                                                <input name="birth_date" type="text" class="form-control" data-provide="datepicker"
                                                    value="{{ \Carbon\Carbon::parse(auth()->user()->metaData->birth_date)->format('d/m/Y') }}">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-bold btn-pure btn-secondary"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-bold btn-pure btn-primary">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="alamat">

                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title"><strong>...</strong></h5>
                                <a class="btn btn-xs btn-primary" data-target="#modal-add-address" data-toggle="modal"
                                    href=""><i class="ti-map-alt"></i> Tambah Alamat</a>
                            </div>

                            <table class="table table-hover table-has-action">
                                <thead>
                                    <tr>
                                        <th>Penerima</th>
                                        <th>No. Handphone</th>
                                        <th>Alamat</th>
                                        <th class="w-80px text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (auth()->user()->addresses != null)
                                    @foreach (auth()->user()->addresses as $key => $address)
                                    <tr class="fs-14 {{ ($address->is_default) ? 'bg-pale-primary' : '' }}">
                                        <td>{{ $address->receiver }}</td>
                                        <td>{{ $address->phone }}</td>
                                        <td>{{ $address->location }}</td>
                                        <td>
                                            <nav class="nav no-gutters gap-2 fs-16">
                                                <a class="nav-link" href="{{ route('address.edit', $address) }}"
                                                    data-provide="tooltip" data-original-title="Edit" data-target="#modal-edit-address-{{ $key }}"
                                                    data-toggle="modal"><i class="ti-pencil"></i></a>

                                                <div class="modal modal-center fade" id="modal-edit-address-{{ $key }}"
                                                    tabindex="-1" aria-hidden="true" style="display: none;">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title"><i class="ti-map-alt"></i> Ubah
                                                                    Alamat</h5>
                                                                <button type="button" class="close" data-dismiss="modal">
                                                                    <span aria-hidden="true">×</span>
                                                                </button>
                                                            </div>
                                                            <form action="{{ route('address.update', $address) }}"
                                                                method="POST">
                                                                @csrf
                                                                @method('PATCH')
                                                                <div class="modal-body">
                                                                    <div class="form-group">
                                                                        <label>Nama Penerima</label>
                                                                        <input name="receiver" class="form-control"
                                                                            type="text" value="{{ $address->receiver }}">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>No. Handphone</label>
                                                                        <input name="phone" class="form-control" type="text"
                                                                            value="{{ $address->phone }}">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Alamat</label>
                                                                        <input name="location" class="form-control"
                                                                            type="text" value="{{ $address->location }}">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="switch switch-border">
                                                                            <input name="is_default" type="checkbox"
                                                                                value="1"
                                                                                {{ $address->is_default ? 'checked' : '' }}>
                                                                            <span class="switch-indicator"></span>
                                                                            <span class="switch-description">Jadikan
                                                                                alamat utama</span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-bold btn-pure btn-secondary"
                                                                        data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-bold btn-pure btn-primary">Simpan</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                                <form class="" action="{{ route('address.destroy', $address) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="nav-link no-border no-padding bg-transparent cursor-pointer"
                                                        type="submit" data-provide="tooltip" data-original-title="Hapus"><i
                                                            class="ti-close"></i></button>
                                                </form>
                                            </nav>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>

                        <div class="modal modal-center fade" id="modal-add-address" tabindex="-1" aria-hidden="true"
                            style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title"><i class="ti-map-alt"></i> Tambah Alamat</h5>
                                        <button type="button" class="close" data-dismiss="modal">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <form action="{{ route('address.store') }}" method="POST">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Nama Penerima</label>
                                                <input name="receiver" class="form-control" type="text">
                                            </div>
                                            <div class="form-group">
                                                <label>No. Handphone</label>
                                                <input name="phone" class="form-control" type="text">
                                            </div>
                                            <div class="form-group">
                                                <label>Alamat</label>
                                                <input name="location" class="form-control" type="text">
                                            </div>
                                            <div class="form-group">
                                                <label class="switch switch-border">
                                                    <input name="is_default" type="checkbox" value="1" checked>
                                                    <span class="switch-indicator"></span>
                                                    <span class="switch-description">Jadikan alamat utama</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-bold btn-pure btn-secondary"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-bold btn-pure btn-primary">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.script')
</body>

</html>