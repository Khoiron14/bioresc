<?php

namespace App\Models\Purchases;

use Illuminate\Database\Eloquent\Model;

use App\Models\Items\Item;
use App\Models\Purchases\Purchase;

class PurchaseType extends Model
{
    const CASH = 1;
    const TRANSFER = 2;
    const COD = 3;

    public $timestamps = false;
    protected $guarded = [];

    public function items()
    {
        return $this->belongsToMany(Item::class);
    }

    public function purchases()
    {
        return $this->hasMany(Purchase::class);
    }
}
