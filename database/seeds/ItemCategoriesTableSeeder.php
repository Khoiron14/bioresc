<?php

use Illuminate\Database\Seeder;
use App\Models\Items\ItemCategory;

class ItemCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ItemCategory::create(['name' => 'Pertanian & Perkebunan']);
        ItemCategory::create(['name' => 'Perternakan']);
        ItemCategory::create(['name' => 'Pakan Ternak']);
        ItemCategory::create(['name' => 'Bibit Tanaman']);
        ItemCategory::create(['name' => 'Bibit Ikan']);
        ItemCategory::create(['name' => 'Buah - buahan']);
        ItemCategory::create(['name' => 'Sayur']);
        ItemCategory::create(['name' => 'Bunga']);
    }
}
