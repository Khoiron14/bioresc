<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes(['verify' => true]);

Route::resource('store', 'Stores\StoreController', ['except' => [
    'index'
]]);

Route::prefix('{store}')->group(function() {
    Route::get('/', 'Stores\StoreController@index')->name('store.index');

    Route::resource('item', 'Items\ItemController', ['except' => [
        'index', 'create', 'edit'
    ]]);
});

Route::namespace('Users')->prefix('/user')->group(function() {
    Route::get('/profile', 'ProfileController@show')->name('user.profile');
    Route::patch('/profile/update', 'ProfileController@update')->name('user.profile.update');
    Route::post('/profile/avatar/update', 'ProfileController@avatar')->name('user.profile.avatar');

    Route::resource('/address', 'AddressController', ['except' => [
        'index', 'create', 'show'
    ]]);

    Route::get('/wishlist', 'WishlistController@index')->name('wishlist.index');
    Route::post('/wishlist/{item}', 'WishlistController@update')->name('wishlist.update');
});