<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bioresc</title>

    <link rel="icon" href="{{ asset('images/component/logo1.png') }}">

    @include('partials.style')
</head>

<body data-provide="pace">
    @include('components.header')

    <div class="p-100">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="d-inline">
                            @if (strlen($store->getImage()) > 1)
                            <img class="avatar avatar-xl" src="{{ $store->getImage() }}">
                            @else
                            <span class="avatar avatar-xl bg-dark fs-25">{{ $store->getImage() }}</span>
                            @endif
                        </div>

                        <h5 class="ml-2 mb-1 d-inline">
                            <a class="hover-primary" href="{{ route('store.index', $store) }}">
                                {{ $store->name }}
                            </a>
                        </h5>

                        <div class="mt-3">
                            <b>Lokasi</b><br>
                            <i class="ti-location-pin"></i>
                            {{ $store->location }}, <b>{{ $store->city }}</b>
                        </div>

                        <div><b>Deskripsi</b><br>{{ $store->description }}</div>
                    </div>

                    @if (auth()->user() && auth()->user()->isOwner($store->user->id))
                    <div class="col-md-6">
                        <button class="btn btn-primary float-md-right" data-target="#modal-add-item" data-toggle="modal"><i
                                class="ti-import"></i> Tambah Barang</button>

                        <div class="modal fade" id="modal-add-item" tabindex="-1" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title"><i class="ti-import"></i> Tambah Barang</h5>
                                        <button type="button" class="close" data-dismiss="modal">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <form action="{{ route('item.store', $store) }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Nama Barang</label>
                                                <input name="name" class="form-control" type="text" value="" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Harga</label>
                                                <input name="price" class="form-control" type="number" min="0" value=""
                                                    required>
                                            </div>
                                            <div class="form-group">
                                                <label>Tipe Satuan</label>
                                                <input name="unit_type" class="form-control" type="text" value=""
                                                    required>
                                            </div>
                                            <div class="form-group">
                                                <label>Status Stok</label>
                                                <select name="stock_status" class="form-control">
                                                    <option value="0">Habis</option>
                                                    <option value="1">Tersedia</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Deskripsi</label>
                                                <textarea name="description" class="form-control" required></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label>Foto</label>
                                                <input name="images[]" class="form-control-file" type="file" value=""
                                                    multiple required>
                                            </div>
                                            <div class="form-group">
                                                    <label>Kategori</label>
                                                    @foreach ($itemCategories as $category)
                                                    <div class="checkbox">
                                                        <input type="checkbox" name="item_categories[]" value="{{ $category->id }}"> {{ $category->name }}
                                                    </div>
                                                    @endforeach
                                                </div>
                                            <div class="form-group">
                                                <label>Tipe Pembayaran</label>
                                                @foreach ($purchaseTypes as $type)
                                                <div class="checkbox">
                                                    <input type="checkbox" name="purchase_types[]" value="{{ $type->id }}"> {{ $type->name }}
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-bold btn-pure btn-secondary"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-bold btn-pure btn-primary">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="card-deck row">
            @foreach ($items as $item)
            <div class="col-md-3">
                <div class="card card-shadowed mx-auto rounded">
                    <a href="{{ route('item.show', [$item->store, $item]) }}">
                        <img class="card-img-top h-200px" style="object-fit:cover" src="{{ asset($item::IMAGE_PATH . $item->images()->first()->path) }}"
                            alt="Item image">
                    </a>
                    <div class="card-body">
                        <a href="{{ route('item.show', [$item->store, $item]) }}">
                            <h3 class="card-title b-0 px-0">{{ $item->name }}</h3>
                        </a>
                        <p><b class="text-success">Rp. {{ number_format($item->price) }}</b> /{{ $item->unit_type }}</p>
                        <small><a class="no-border" href="{{ route('store.index', $item->store) }}">{{
                                $item->store->name }}</a></small>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    @include('partials.script')
</body>

</html>