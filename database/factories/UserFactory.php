<?php

use Faker\Generator as Faker;
use App\Models\Users\User;
use App\Models\Users\UserMetaData as MetaData;
use App\Models\Image;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'username' => $faker->userName,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => Carbon::now()->toDateTimeString(),
        'password' => bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->afterCreating(User::class, function ($user, $faker) {
    $user->image()->save(factory(Image::class)->states('user')->make());
    $user->metaData()->save(factory(MetaData::class)->make());
});